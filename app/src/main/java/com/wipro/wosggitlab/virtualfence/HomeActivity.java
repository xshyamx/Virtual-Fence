package com.wipro.wosggitlab.virtualfence;

import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;

/**
 * The HomeActivity class is the main homepage of the application
 * It basically gives two option to choose for Caregiver or cared
 *
 * @author sam
 * @version 1.0
 * @since 2015-07-25
 */

public class HomeActivity extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        final Button button_caregiver = (Button) findViewById(R.id.button_caregiver);
        button_caregiver.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v){
                Log.e("SAM", "Logging Button");
                Intent caregiver_intent = new Intent(HomeActivity.this, Caregiver.class);
                startActivity(caregiver_intent);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_home, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
