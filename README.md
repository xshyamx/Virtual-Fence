# Virutal Fence

Virtual-Fence is an application to monitor children and elderly people using geofenced locations. If the children or elderly people go out of the geofence set by the caregiver, the caregivers are notified, and also alerting the children or elderly person. A cared person (child or elderly) can be monitored by multiple caregivers, and also a caregiver can monitor multiple cared persons.

The project currently makes use of android devices and GCM server. Android device's GPS technology is used for location tracking, and GCM server is used as a communication bridge between the cared device and the caregiver device.
